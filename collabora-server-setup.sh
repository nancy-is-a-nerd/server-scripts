#!/usr/bin/env bash

# File: collabora-server-setup.sh
# ---------------------
# Quick setup to install and configure a standalone server for
# Collabora Online Development Edition (CODE) Server on Debian using 
# nginx as a reverse proxy. 
# 
# Find the full article here:
# https://nancyisanerd.com/setup-collabora-online-for-nextcloud-on-debian-without-docker/
#
# Setup time: < 2 mins.
#
# Current Debian version: Bullseye (11)
# Runs great on a $5 Linode, or a $5 Digital Ocean droplet.
#
# https://www.collaboraoffice.com/code/
# https://www.collaboraoffice.com/code/linux-packages/
# https://sdk.collaboraonline.com/docs/installation/index.html
#
# Author: Nancy B.
# Created: 2022-09-03
# Last Modified: 2022-09-03
#
# =============================================================================

echo -n "Enter your collabora host TLD (this server) "
read COLLABORA_HOST
echo -n "Enter your nextcloud host TLD "
read NEXTCLOUD_HOST
echo -n "Enter email address to use for SSL certificate "
read SSL_EMAIL

# =============================================================================

# Import the signing keys
# 
cd /usr/share/keyrings
wget https://collaboraoffice.com/downloads/gpg/collaboraonline-release-keyring.gpg

# Add CODE package repositories
# -----------------------------
echo -e "Types: deb\nURIs: https://www.collaboraoffice.com/repos/CollaboraOnline/CODE-debian11\nSuites: ./\nSigned-By: /usr/share/keyrings/collaboraonline-release-keyring.gpg" > /etc/apt/sources.list.d/collaboraonline.sources

# Update and install packages
# ----------------------------
apt update && sudo apt install coolwsd code-brand -y

# Configure coolwsd
#------------------
cp /etc/coolwsd/coolwsd.xml /etc/coolwsd/coolwsd.xml.orig
coolconfig set ssl.enable false
coolconfig set ssl.termination true
coolconfig set net.listen loopback
coolconfig set storage.wopi.host $NEXTCLOUD_HOST

# This has user input
coolconfig set-admin-password

# Restart Collabora coolwsd
#--------------------------
systemctl restart coolwsd

# Install nginx
apt install nginx -y

# Set up Let's Encrypt SSL
apt install certbot python3-certbot-nginx -y
sudo certbot --nginx --agree-tos --redirect --hsts --staple-ocsp --email $SSL_EMAIL -d $COLLABORA_HOST

# Configure nginx reverse proxy
#------------------------------
echo -e "server {
  listen 443 ssl http2;
  server_name CHANGE_ME;

  error_log /var/log/nginx/collabora.error;

  ssl_certificate /etc/letsencrypt/live/CHANGE_ME/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/CHANGE_ME/privkey.pem;

  # static files
  location ^~ /browser {
    proxy_pass http://localhost:9980;
    proxy_set_header Host \$http_host;
  }

  # WOPI discovery URL
  location ^~ /hosting/discovery {
    proxy_pass http://localhost:9980;
    proxy_set_header Host \$http_host;
  }

  # Capabilities
  location ^~ /hosting/capabilities {
    proxy_pass http://localhost:9980;
    proxy_set_header Host \$http_host;
  }

  # main websocket
  location ~ ^/cool/(.*)/ws$ {
    proxy_pass http://localhost:9980;
    proxy_set_header Upgrade \$http_upgrade;
    proxy_set_header Connection \"Upgrade\";
    proxy_set_header Host \$http_host;
    proxy_read_timeout 36000s;
  }

  # download, presentation and image upload
  location ~ ^/(c|l)ool {
    proxy_pass http://localhost:9980;
    proxy_set_header Host \$http_host;
  }

  # Admin Console websocket
  location ^~ /cool/adminws {
    proxy_pass http://localhost:9980;
    proxy_set_header Upgrade \$http_upgrade;
    proxy_set_header Connection \"Upgrade\";
    proxy_set_header Host \$http_host;
    proxy_read_timeout 36000s;
  }
}" > /etc/nginx/sites-available/collabora.conf
sed -i "s/CHANGE_ME/$COLLABORA_HOST/g" /etc/nginx/sites-available/collabora.conf

# Enable collabora in nginx and restart 
# (remove default to avoid issues)
#--------------------------------------
ln -s /etc/nginx/sites-available/collabora.conf /etc/nginx/sites-enabled/collabora.conf
rm /etc/nginx/sites-enabled/default && rm /etc/nginx/sites-available/default
systemctl restart nginx

# Verify we're up and running
#----------------------------
systemctl status coolwsd
systemctl status nginx

echo -e "\n-------------------------------------------------------------------------------
Find your Collabora Admin Console here: 
https://$COLLABORA_HOST/browser/dist/admin/admin.html
-------------------------------------------------------------------------------"
