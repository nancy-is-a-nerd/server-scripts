# Scripts to Make Server Setup a Snap

## Collabora Online Development Edition (CODE)
https://www.collaboraoffice.com/code/


Edit office documents in your browser by integrating Collabora into your [Nextcloud](https://nextcloud.com/). This script is a push-button companion to the parent article on my tech blog: [https://nancyisanerd.com/setup-collabora-online-for-nextcloud-on-debian-without-docker](https://nancyisanerd.com/setup-collabora-online-for-nextcloud-on-debian-without-docker)


